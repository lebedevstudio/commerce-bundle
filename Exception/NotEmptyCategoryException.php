<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Exception;

use Throwable;

class NotEmptyCategoryException extends \RuntimeException
{
    protected $message = 'Cannot delete not empty category';
    protected $code = 4000;
}
