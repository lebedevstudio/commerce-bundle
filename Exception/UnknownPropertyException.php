<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Exception;

use Throwable;

class UnknownPropertyException extends \RuntimeException
{
    protected $message = 'Unknown property %s';
    protected $code = 4001;

    public function __construct(string $property)
    {
        $this->message = sprintf($this->message, $property);
        parent::__construct($this->message, $this->code, null);
    }
}
