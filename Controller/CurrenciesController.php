<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CommerceBundle\Entity\Currency;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CurrenciesController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/commerce/currencies", name="commerce.currencies.list", methods={"GET"})
     */
    public function getCurrenciesList() : JsonResponse
    {
        return $this->list(Currency::class, Currency::MULTIPLE_KEY);
    }

    /**
    * @Route("/commerce/currencies", name="commerce.currencies.create", methods={"POST"})
    *
    * @return JsonResponse
    */
    public function createCurrency() : JsonResponse
    {
        return $this->persist(Currency::class, Currency::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }
}
