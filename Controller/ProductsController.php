<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CommerceBundle\Entity\Category;
use lst\CommerceBundle\Entity\Product;
use lst\CommerceBundle\Repository\CategoryRepository;
use lst\CommerceBundle\Repository\ProductRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Service\Breadcrumbs\Breadcrumbs;
use lst\CoreBundle\Service\Breadcrumbs\Crumb;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProductsController extends AbstractController
{
    /** @var CategoryRepository */
    private $categoryRepository;
    /** @var ProductRepository */
    private $productRepository;
    /** @var Operations */
    protected $operations;

    /** @var string */
    private $entity = Product::class;
    /** @var string */
    private $entitySingleKey = Product::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Product::MULTIPLE_KEY;

    public function __construct(
        Operations $operations,
        NormalizerInterface $normalizer,
        RequestStack $request,
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository)
    {
        $this->operations = $operations;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products",
     *     name="commerce.product.get.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/commerce/products/{id}",
     *     name="commerce.product.get.id",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Product $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntityById(Product $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ]),
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($entity), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($entity), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/alias/{alias}",
     *     name="commerce.product.get.alias",
     *     methods={"GET"},
     *     requirements={"alias"="[\w\-]+"}
     * )
     *
     * @param Product $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntityByAlias(Product $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity,'array', [
                'groups' => $this->serializationGroups
            ]),
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($entity), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($entity), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/{id}",
     *     name="commerce.product.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Product $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Product $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products",
     *     name="commerce.product.create",
     *     methods={"POST"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/{id}",
     *     name="commerce.product.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Product $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Product $entity): JsonResponse
    {
        return $this->delete($entity);
    }

    /**
     * @param  EntityTypeInterface  $entityTypeInterface
     * @return array
     */
    private function collectBreadcrumbs(EntityTypeInterface $entityTypeInterface): array
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->addCrumb(new Crumb(
                $entityTypeInterface->getId(),
                $entityTypeInterface->getTitle(),
                $entityTypeInterface->getAlias(),
                $entityTypeInterface::getEntityTypeId()
            )
        );
        $parents = $this->categoryRepository->getParents($entityTypeInterface->getCategory());
        /** @var Category $category */
        foreach ($parents as $category) {
            $breadcrumbs->addCrumb(new Crumb(
                    $category->getId(),
                    $category->getTitle(),
                    $category->getAlias(),
                    Category::getEntityTypeId()
                )
            );
        }

        return $breadcrumbs->getCrumbs();
    }
}
