<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\Documentation;
use lst\CommerceBundle\Entity\Product;
use lst\CommerceBundle\Repository\ProductRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProductsDocumentationController extends AbstractController
{
    /** @var ProductRepository */
    private $productRepository;
    /** @var Operations */
    protected $operations;

    public function __construct(
        Operations $operations,
        NormalizerInterface $normalizer,
        RequestStack $request,
        ProductRepository $productRepository)
    {
        $this->operations = $operations;
        $this->productRepository = $productRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/documentation/{documentation}",
     *     name="commerce.product.documentation.add",
     *     methods={"POST"},
     *     requirements={"product"="\d+", "documentation"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Product $product
     * @param Documentation $documentation
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addDocumentationToProduct(Product $product, Documentation $documentation): JsonResponse
    {
        $product->getDocumentations()->add($documentation);
        $this->productRepository->persist($product);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/documentation/{documentation}",
     *     name="commerce.product.documentation.unlink",
     *     methods={"DELETE"},
     *     requirements={"product"="\d+", "documentation"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Documentation $documentation
     * @param Product $product
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unlinkDocumentationFromProduct(Documentation $documentation, Product $product): JsonResponse
    {
        $product->getDocumentations()->removeElement($documentation);
        $this->productRepository->persist($product);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }
}