<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CommerceBundle\Entity\DocumentationGroup;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DocumentationGroupController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = DocumentationGroup::class;
    /** @var string */
    private $entitySingleKey = DocumentationGroup::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = DocumentationGroup::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/groups",
     *     name="commerce.documentation.groups.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/groups/{id}",
     *     name="commerce.documentation.groups.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param DocumentationGroup $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(DocumentationGroup $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/groups",
     *     name="commerce.documentation.groups.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/groups/{id}",
     *     name="commerce.documentation.groups.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param DocumentationGroup $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(DocumentationGroup $entity): JsonResponse
    {
        return $this->persist(
            DocumentationGroup::class,
            DocumentationGroup::SINGLE_KEY,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/groups/{id}",
     *     name="commerce.documentation.groups.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param DocumentationGroup $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(DocumentationGroup $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}