<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CommerceBundle\Entity\ProductType;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ProductsTypesController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/commerce/products/types",
     *     name="commerce.product.type.list",
     *     methods={"GET"}
     * )
     */
    public function getProductTypesList() : JsonResponse
    {
        return $this->list(ProductType::class, ProductType::MULTIPLE_KEY);
    }

    /**
     * @Route("/commerce/products/types/{type}",
     *     name="commerce.product.type.get",
     *     methods={"GET"},
     *     requirements={"type"="\d+"}
     * )
     */
    public function getProductType(ProductType $type) : JsonResponse
    {
        return new JsonResponse([
            ProductType::SINGLE_KEY => $this->normalizer->normalize($type, 'array', [
                'groups' => $this->serializationGroups,
            ])
        ]);
    }

    /**
    * @Route("/commerce/products/types",
    *     name="commerce.product.type.create",
    *     methods={"POST"}
    * )
    *
    * @return JsonResponse
    */
    public function createProductType() : JsonResponse
    {
        return $this->persist(ProductType::class, ProductType::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/commerce/products/types/{type}",
     *     name="commerce.product.type.update",
     *     methods={"PUT"},
     *     requirements={"type"="\d+"}
     * )
     *
     * @return JsonResponse
     */
    public function updateProductType() : JsonResponse
    {
        return $this->persist(ProductType::class, ProductType::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/commerce/products/types/{type}",
     *     name="commerce.product.type.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param ProductType $productType
     * @return JsonResponse
     */
    public function deleteProductType(ProductType $type) : JsonResponse
    {
        return $this->delete($type);
    }
}
