<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\Product;
use lst\CommerceBundle\Entity\ProductProperty;
use lst\CommerceBundle\Repository\ProductPropertyRepository;
use lst\CommerceBundle\Repository\ProductRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProductsPropertiesController extends AbstractController
{
    /** @var ProductRepository */
    private $productRepository;
    private $productPropertyRespository;
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entityClass = ProductProperty::class;
    /** @var string */
    private $entitySingleKey = ProductProperty::SINGLE_KEY;

    public function __construct(
        Operations $operations,
        NormalizerInterface $normalizer,
        RequestStack $request,
        ProductRepository $productRepository,
        ProductPropertyRepository $productPropertyRepository
    )
    {
        $this->operations = $operations;
        $this->productRepository = $productRepository;
        $this->productPropertyRespository = $productPropertyRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/properties",
     *     name="commerce.product.property.add",
     *     requirements={"product"="\d+", "property"="\d+"},
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  Product  $product
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function addProductProperty(Product $product) : JsonResponse
    {
        return $this->persist(
            $this->entityClass,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route("/commerce/products/{product}/properties/{productProperty}",
     *     name="commerce.product.property.refresh",
     *     requirements={"product"="\d+", "productProperty"="\d+"},
     *     methods={"PUT"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  Product  $product
     * @param  ProductProperty  $property
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function refreshProductProperty(Product $product, ProductProperty $property): JsonResponse
    {
        return $this->persist(
            $this->entityClass,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/properties/{productProperty}",
     *     name="commerce.product.property.unlink",
     *     requirements={"product"="\d+", "productProperty"="\d+"},
     *     methods={"DELETE"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  Product  $product
     * @param  ProductProperty  $productProperty
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unlinkPropertyFromProduct(Product $product, ProductProperty $productProperty): JsonResponse
    {
//        $product->getProperties()->removeElement($productProperty);
//        $this->productRepository->persist($product);

        // Zaebalo fix this
        $this->productPropertyRespository->delete($productProperty);

        return new JsonResponse([
            'result' => 'OK'
        ], 200);
    }
}
