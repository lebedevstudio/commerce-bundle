<?php
declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use http\Exception\RuntimeException;
use lst\CommerceBundle\Entity\PropertyGroup;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class PropertyGroupController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = PropertyGroup::class;
    /** @var string */
    private $entitySingleKey = PropertyGroup::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = PropertyGroup::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;
        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/properties/groups",
     *     name="commerce.properties.groups.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/commerce/properties/groups/{id}",
     *     name="commerce.properties.groups.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param PropertyGroup $entity
     *
     * @return JsonResponse
     */
    public function getEntity(PropertyGroup $entity): JsonResponse
    {
        return new JsonResponse([$this->entitySingleKey => $this->normalizer->normalize($entity, 'array', ['groups' => $this->serializationGroups])], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/properties/groups",
     *     name="commerce.properties.groups.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist($this->entity, $this->entitySingleKey, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route(
     *     "/commerce/properties/groups/{id}",
     *     name="commerce.properties.groups.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param PropertyGroup $entity
     *
     * @return JsonResponse
     */
    public function updateEntity(PropertyGroup $entity): JsonResponse
    {
        return $this->persist($this->entity, $this->entitySingleKey, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route(
     *     "/commerce/properties/groups/{id}",
     *     name="commerce.properties.groups.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param PropertyGroup $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(PropertyGroup $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}