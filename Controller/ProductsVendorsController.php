<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CommerceBundle\Entity\ProductVendor;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ProductsVendorsController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/commerce/products/vendors",
     *     name="commerce.product.vendor.list",
     *     methods={"GET"}
     * )
     * @return JsonResponse
     */
    public function getProductVendorsList() : JsonResponse
    {
        return $this->list(ProductVendor::class, ProductVendor::MULTIPLE_KEY);
    }

    /**
     * @Route("/commerce/products/vendors/{vendor}",
     *     name="commerce.product.vendor.get",
     *     methods={"GET"}
     * )
     * @param ProductVendor $vendor
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getProductVendor(ProductVendor $vendor) : JsonResponse
    {
        return new JsonResponse([
            ProductVendor::SINGLE_KEY => $this->normalizer->normalize($vendor, 'array', [
                'groups' => $this->serializationGroups,
            ])
        ]);
    }

    /**
     * @Route("/commerce/products/vendors",
     *     name="commerce.product.vendor.create",
     *     methods={"POST"}
     * )
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createProductVendor() : JsonResponse
    {
        return $this->persist(ProductVendor::class, ProductVendor::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/commerce/products/vendors/{vendor}",
     *     name="commerce.product.vendor.update",
     *     methods={"PUT"}
     * )
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateProductVendor() : JsonResponse
    {
        return $this->persist(ProductVendor::class, ProductVendor::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/commerce/products/vendors/{vendor}",
     *     name="commerce.product.vendor.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param ProductVendor $vendor
     * @return JsonResponse
     */
    public function deleteCategory(ProductVendor $vendor) : JsonResponse
    {
        return $this->delete($vendor);
    }
}
