<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CommerceBundle\Entity\Documentation;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DocumentationController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = Documentation::class;
    /** @var string */
    private $entitySingleKey = Documentation::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Documentation::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation",
     *     name="commerce.documentation.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/{id}",
     *     name="commerce.documentation.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Documentation $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(Documentation $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/documentation",
     *     name="commerce.documentation.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/{id}",
     *     name="commerce.documentation.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Documentation $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Documentation $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/products/documentation/{id}",
     *     name="commerce.documentation.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Documentation $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Documentation $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}