<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\Product;
use lst\CommerceBundle\Repository\ProductRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use lst\MediaBundle\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProductsAttachmentController extends AbstractController
{
    /** @var ProductRepository */
    private $productRepository;
    /** @var Operations */
    protected $operations;

    public function __construct(
        Operations $operations,
        NormalizerInterface $normalizer,
        RequestStack $request,
        ProductRepository $productRepository)
    {
        $this->operations = $operations;
        $this->productRepository = $productRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/attach/{attach}",
     *     name="commerce.product.attach.add",
     *     methods={"POST"},
     *     requirements={"product"="\d+", "attach"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  Product  $product
     * @param  File  $attach
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAttachmentToProduct(Product $product, File $attach): JsonResponse
    {
        $product->getAttaches()->add($attach);
        $this->productRepository->persist($product);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/products/{product}/attach/{attach}",
     *     name="commerce.product.attach.unlink",
     *     methods={"DELETE"},
     *     requirements={"product"="\d+", "attach"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  File  $attach
     * @param  Product  $product
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unlinkAttachmentFromProduct(File $attach, Product $product): JsonResponse
    {
        $product->getAttaches()->removeElement($attach);
        $product->getAttaches()->remove($attach->getId());
        $this->productRepository->persist($product);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }
}