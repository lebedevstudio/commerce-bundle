<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Controller;

use lst\CommerceBundle\Entity\Property;
use lst\CommerceBundle\Repository\PropertyRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PropertiesController extends AbstractController
{
    /** @var PropertyRepository */
    private $propertyRepository;
    /** @var Operations */
    protected $operations;

    /** @var string */
    private $entity = Property::class;
    /** @var string */
    private $entitySingleKey = Property::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Property::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request,
        PropertyRepository $propertyRepository)
    {
        $this->operations = $operations;
        $this->propertyRepository = $propertyRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/commerce/properties/comparison",
     *     name="commerce.product.property.comparison.map",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function getEntityComparisonList() : JsonResponse
    {
        return new JsonResponse([
            'equal', 'equal_or_less', 'equal_or_more', 'less', 'more'
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/properties",
     *     name="commerce.product.property.get.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/commerce/properties/{id}",
     *     name="commerce.product.property.get.id",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     * @param Property $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntityById(Property $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/commerce/properties",
     *     name="commerce.product.property.create",
     *     methods={"POST"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/properties/{id}",
     *     name="commerce.product.property.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param  Property  $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Property $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/commerce/properties/{id}",
     *     name="commerce.product.property.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Property $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Property $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}
