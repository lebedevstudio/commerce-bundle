<?php

namespace lst\CommerceBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /** @var EntityManager */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Product::class);
    }

    /**
     * @param Product $product
     *
     * @return Product
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Product $product) : Product
    {
        $entry = $this->em->merge($product);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param Product $product
     *
     * @return Product
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Product $product) : Product
    {
        $this->em->remove($product);
        $this->em->flush();

        return $product;
    }
}
