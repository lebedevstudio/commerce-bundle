<?php

namespace lst\CommerceBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\Documentation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Documentation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Documentation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Documentation[]    findAll()
 * @method Documentation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentationRepository extends ServiceEntityRepository
{
    /** @var EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Documentation::class);
    }

    /**
     * @param Documentation $documentation
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Documentation $documentation)
    {
        $this->em->persist($documentation);
        $this->em->flush();
    }

    /**
     * @param Documentation $documentation
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Documentation $documentation) : void
    {
        $this->em->remove($documentation);
        $this->em->flush();
    }
}