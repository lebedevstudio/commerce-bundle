<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\ProductVendor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductVendor|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVendor|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVendor[]    findAll()
 * @method ProductVendor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVendorRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, ProductVendor::class);
    }

    /**
     * @param ProductVendor $productVendor
     *
     * @return ProductVendor
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(ProductVendor $productVendor) : ProductVendor
    {
        $entry = $this->em->merge($productVendor);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param ProductVendor $productVendor
     *
     * @return ProductVendor
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(ProductVendor $productVendor) : ProductVendor
    {
        $this->em->remove($productVendor);
        $this->em->flush();

        return $productVendor;
    }
}
