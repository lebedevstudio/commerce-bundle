<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductType[]    findAll()
 * @method ProductType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductTypeRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, ProductType::class);
    }

    /**
     * @param ProductType $productType
     *
     * @return ProductType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(ProductType $productType) : ProductType
    {
        $entry = $this->em->merge($productType);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param ProductType $productType
     *
     * @return ProductType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(ProductType $productType) : ProductType
    {
        $this->em->remove($productType);
        $this->em->flush();

        return $productType;
    }
}
