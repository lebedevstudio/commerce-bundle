<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Currency|null find($id, $lockMode = null, $lockVersion = null)
 * @method Currency|null findOneBy(array $criteria, array $orderBy = null)
 * @method Currency[]    findAll()
 * @method Currency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Currency::class);
    }

    /**
     * @param Currency $currency
     *
     * @return Currency
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Currency $currency) : Currency
    {
        $entry = $this->em->merge($currency);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param Currency $currency
     *
     * @return Currency
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Currency $currency) : Currency
    {
        $this->em->remove($currency);
        $this->em->flush();

        return $currency;
    }
}
