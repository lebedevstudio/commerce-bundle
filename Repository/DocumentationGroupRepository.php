<?php

namespace lst\CommerceBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CommerceBundle\Entity\DocumentationGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentationGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentationGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentationGroup[]    findAll()
 * @method DocumentationGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentationGroupRepository extends ServiceEntityRepository
{
    /** @var EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, DocumentationGroup::class);
    }

    /**
     * @param DocumentationGroup $documentationGroup
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(DocumentationGroup $documentationGroup)
    {
        $this->em->persist($documentationGroup);
        $this->em->flush();
    }

    /**
     * @param DocumentationGroup $documentationGroup
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(DocumentationGroup $documentationGroup) : void
    {
        $this->em->remove($documentationGroup);
        $this->em->flush();
    }
}