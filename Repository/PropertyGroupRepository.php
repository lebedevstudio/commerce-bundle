<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\PropertyGroup;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PropertyGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyGroup[]    findAll()
 * @method PropertyGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyGroupRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, PropertyGroup::class);
    }

    /**
     * @param PropertyGroup $propertyGroup
     *
     * @return PropertyGroup
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(PropertyGroup $propertyGroup) : PropertyGroup
    {
        $entry = $this->em->merge($propertyGroup);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param PropertyGroup $propertyGroup
     *
     * @return PropertyGroup
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(PropertyGroup $propertyGroup) : PropertyGroup
    {
        $this->em->remove($propertyGroup);
        $this->em->flush();

        return $propertyGroup;
    }
}