<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\ProductProperty;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductProperty|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductProperty|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductProperty[]    findAll()
 * @method ProductProperty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPropertyRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, ProductProperty::class);
    }

    /**
     * @param ProductProperty $productFeature
     *
     * @return ProductProperty
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(ProductProperty $productFeature) : ProductProperty
    {
        $entry = $this->em->merge($productFeature);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param ProductProperty $productFeature
     *
     * @return ProductProperty
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(ProductProperty $productFeature) : ProductProperty
    {
        $this->em->remove($productFeature);
        $this->em->flush();

        return $productFeature;
    }
}
