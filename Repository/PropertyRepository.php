<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Property::class);
    }

    /**
     * @param Property $property
     *
     * @return Property
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Property $property) : Property
    {
        $entry = $this->em->merge($property);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param Property $property
     *
     * @return Property
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Property $property) : Property
    {
        $this->em->remove($property);
        $this->em->flush();

        return $property;
    }
}
