<?php

namespace lst\CommerceBundle\Repository;

use lst\CommerceBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Category::class);
    }

    /**
     * @param Category $category
     *
     * @return Category
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Category $category) : Category
    {
        $entry = $this->em->merge($category);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param Category $category
     *
     * @return Category
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Category $category) : Category
    {
        $this->em->remove($category);
        $this->em->flush();

        return $category;
    }

    public function getParents(?Category $category, $parents = []) : array
    {
        if ($category != null) {
            $parents[] = $category;
            return $this->getParents($category->getParent(), $parents);
        }

        return $parents;
    }
}
