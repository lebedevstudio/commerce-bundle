<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product Property Entity
 *
 * @ORM\Table(name="commerce_products_properties")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\ProductPropertyRepository")
 */
class ProductProperty extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 14;
    /** @var string */
    public const SINGLE_KEY = 'productProperty';
    /** @var string */
    public const MULTIPLE_KEY = 'productPropertys';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productProperty")
     */
    protected $product;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Property")
     * @Groups({"basic"})
     */
    protected $property;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */

    protected $value = '';
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic"})
     */
    protected $comparison = '';

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Property
     */
    public function getProperty() : Property
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty(Property $property) : void
    {
        $this->property = $property;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product) : void
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct() : Product
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value) : void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getComparison() : string
    {
        return $this->comparison;
    }

    /**
     * @param string $comparison
     */
    public function setComparison(string $comparison) : void
    {
        $this->comparison = $comparison;
    }
}
