<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Entity\Gallery;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\CoreBundle\Entity\PageMeta;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category Entity
 *
 * @ORM\Table(name="commerce_categories")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\CategoryRepository")
 */
class Category extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 3;
    /** @var string */
    public const SINGLE_KEY = 'category';
    /** @var string */
    public const MULTIPLE_KEY = 'categories';
    /** @var bool */
    private $showInActive = false;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Asserts\UniqueField(
     *     with="locale"
     * )
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic"})
     */
    protected $alias;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    protected $title;

    /**
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"basic"})
     */

    protected $subtitle = '';
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Groups({"basic"})
     */
    protected $description = '';

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File", cascade={"persist"})
     * @Groups({"image"})
     */
    protected $image = null;

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\Gallery")
     * @Groups({"gallery"})
     */
    protected $gallery = null;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children", cascade={"persist"}, fetch="EAGER")
     * @Groups({"parent"})
     * @MaxDepth(1)
     */
    protected $parent = null;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", fetch="EXTRA_LAZY")
     * @MaxDepth(1)
     * @Groups({"children"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category", fetch="EXTRA_LAZY")
     * @MaxDepth(1)
     * @Groups({"products"})
     */
    private $products;

    /**
     * @ORM\OneToOne(targetEntity="lst\CoreBundle\Entity\PageMeta")
     * @Groups({"basic"})
     */
    protected $pageMeta = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias) : void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getSubtitle() : ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle) : void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File|string $image
     */
    public function setImage(?File $image) : void
    {
        $this->image = $image;
    }
    
    /**
     * @return Gallery|null
     */
    public function getGallery() : ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery|null $gallery
     */
    public function setGallery(?Gallery $gallery) : void
    {
        $this->gallery = $gallery;
    }

    /**
     * @return Category|null
     */
    public function getParent() : ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category|null $parent
     */
    public function setParent(?Category $parent) : void
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        $criteria = Criteria::create();
        if ($this->showInActive != true) {
            $criteria->where(Criteria::expr()->eq('active', true));
        }

        return $this->children->matching($criteria);
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        $criteria = Criteria::create();
        if ($this->showInActive != true) {
            $criteria->where(Criteria::expr()->eq('active', true));
        }

        return $this->products->matching($criteria);
    }

    /**
     * @param bool $show
     */
    public function showInActive(bool $show = true) : void
    {
        $this->showInActive = $show;
    }

    /**
     * @return PageMeta|null
     */
    public function getPageMeta()
    {
        return $this->pageMeta;
    }

    /**
     * @param PageMeta|null $pageMeta
     */
    public function setPageMeta(?PageMeta $pageMeta) : void
    {
        $this->pageMeta = $pageMeta;
    }
}
