<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\MediaBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documentation Entity
 *
 * @ORM\Table(name="commerce_documentations")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\DocumentationRepository")
 */
class Documentation extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 23;
    /** @var string */
    public const SINGLE_KEY = 'documentation';
    /** @var string */
    public const MULTIPLE_KEY = 'documentations';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\Length(max=255)
     * @Assert\NotBlank()
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false
     * )
     *
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @Assert\Type(
     *     type="lst\CommerceBundle\Entity\DocumentationGroup"
     * )
     *
     * @ORM\ManyToOne(
     *     targetEntity="lst\CommerceBundle\Entity\DocumentationGroup",
     *     inversedBy="documentation"
     * )
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"group"})
     **/
    private $group;

    /**
     * @ORM\OneToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"file"})
     **/
    private $file = null;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="lst\CommerceBundle\Entity\Product",
     *     mappedBy="documentations"
     * )
     * @Groups({"products"})
     */
    private $products;


    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DocumentationGroup
     */
    public function getGroup(): DocumentationGroup
    {
        return $this->group;
    }

    /**
     * @param DocumentationGroup $group
     */
    public function setGroup(DocumentationGroup $group): void
    {
        $this->group = $group;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     */
    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }
}