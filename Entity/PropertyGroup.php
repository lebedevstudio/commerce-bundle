<?php
declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use Symfony\Component\Serializer\Annotation\Groups;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use lst\MediaBundle\Entity\File;

/**
 * PropertyGroup Entity
 *
 * @ORM\Table(
 *     name="commerce_properties_groups",
 *     indexes={
 *          @ORM\Index(name="IDX_COMMERCE_PROPERTIES_GROUP_LOCALE_INDEX", columns={"locale"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\PropertyGroupRepository")
 */
class PropertyGroup extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 21;
    /** @var string */
    public const SINGLE_KEY = 'propertyGroup';
    /** @var string */
    public const MULTIPLE_KEY = 'propertyGroups';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    private $title = '';

    /**
     * @Assert\Type(
     *     type="File"
     * )
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"image"})
     */
    private $image = null;

    /**
     * @Assert\Type(
     *     type="Property"
     * )
     * @ORM\OneToMany(targetEntity="Property", mappedBy="group")
     * @Groups({"properties"})
     */
    private $properties;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }
}