<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use Doctrine\Common\Collections\Collection;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Entity\Gallery;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Entity\PageMeta;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product Entity
 *
 * @ORM\Table(name="commerce_products")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\ProductRepository")
 */
class Product extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 4;
    /** @var string */
    public const SINGLE_KEY = 'product';
    /** @var string */
    public const MULTIPLE_KEY = 'products';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic", "full"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Assert\Type(type="string")
     * @Asserts\UniqueField(
     *     with="locale"
     * )
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "full"})
     */
    protected $alias;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Assert\Type(type="string")
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic", "full"})
     */
    protected $title;

    /**
     * @Assert\Type(
     *     type="string"
     * )
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"basic", "full"})
     */
    protected $subtitle = '';

    /**
     * @Assert\Type(type="string")
     * @ORM\Column(type="text", options={"default":""})
     * @Groups({"basic", "full"})
     */
    protected $description = '';

    /**
     * @Assert\Type(type="string")
     * @ORM\Column(type="text", options={"default":""})
     * @Groups({"basic", "full"})
     */
    protected $content = '';

    /**
     * @Assert\Type(
     *     type="lst\MediaBundle\Entity\File"
     * )
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"image", "full"})
     */
    protected $image = null;

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\Gallery")
     * @Groups({"gallery", "full"})
     */
    protected $gallery = null;

    /**
     * @Assert\Valid()
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @MaxDepth(1)
     * @Groups({"category", "full"})
     */
    protected $category;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     * @Groups({"basic", "full"})
     */
    protected $detailView = true;

    /**
     * @ORM\ManyToMany(targetEntity="lst\MediaBundle\Entity\File", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="commerce_products_attaches",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="attach_id", referencedColumnName="id")}
     * )
     * @Groups({"attaches", "full"})
     */
    protected $attaches;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="ProductProperty", mappedBy="product", cascade={"all"})
     * @Groups({"properties", "full"})
     */
    protected $properties;

    /**
     * @ORM\Column(type="float", precision=2, nullable=false)
     * @Assert\Type(
     *     type="float",
     *     message="expected {{ type }} but got {{ value }}"
     * )
     * @Groups({"basic"})
     */
    protected $price = 0.00;

    /**
     * @ORM\ManyToOne(targetEntity="lst\CommerceBundle\Entity\Currency", cascade={"persist"})
     * @Assert\Valid()
     * @Groups({"basic"})
     */
    protected $currency;

    /**
     * @ORM\OneToOne(targetEntity="lst\CoreBundle\Entity\PageMeta")
     * @Groups({"basic"})
     */
    protected $pageMeta = null;

    /**
     * @Assert\Type(type="string")
     * @ORM\Column(type="text", options={"default":""})
     * @Groups({"basic", "full"})
     */
    protected $serial = '';

    /**
     * @ORM\ManyToOne(targetEntity="ProductVendor")
     * @Groups({"basic"})
     */
    protected $vendor = null;

    /**
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @Groups({"basic"})
     */
    protected $type = null;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="lst\CommerceBundle\Entity\Documentation",
     *     inversedBy="products"
     * )
     * @Groups({"documentations"})
     */
    private $documentations;

    public function __construct()
    {
        $this->attaches = new ArrayCollection();
        $this->properties = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->documentations = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias) : void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getSubtitle() : string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle) : void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image) : void
    {
        $this->image = $image;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery() : ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery|null $gallery
     */
    public function setGallery(?Gallery $gallery) : void
    {
        $this->gallery = $gallery;
    }

    public function getCategory() : Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category) : void
    {
        $this->category = $category;
    }

    /**
     * @return bool
     */
    public function getDetailView() : bool
    {
        return $this->detailView;
    }

    /**
     * @param bool $detailView
     */
    public function setDetailView(bool $detailView) : void
    {
        $this->detailView = $detailView;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttaches()
    {
        return $this->attaches;
    }

    /**
     * @return ArrayCollection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @param $price
     */
    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency(?Currency $currency) : void
    {
        $this->currency = $currency;
    }

    /**
     * @return PageMeta|null
     */
    public function getPageMeta()
    {
        return $this->pageMeta;
    }

    /**
     * @param PageMeta|null $pageMeta
     */
    public function setPageMeta(?PageMeta $pageMeta) : void
    {
        $this->pageMeta = $pageMeta;
    }

    /**
     * @return string
     */
    public function getSerial() : string
    {
        return $this->serial;
    }

    /**
     * @param string $serial
     */
    public function setSerial(string $serial) : void
    {
        $this->serial = $serial;
    }

    /**
     * @return ProductVendor|null
     */
    public function getVendor() : ?ProductVendor
    {
        return $this->vendor;
    }

    /**
     * @param ProductVendor|null $vendor
     */
    public function setVendor(?ProductVendor $vendor) : void
    {
        $this->vendor = $vendor;
    }

    /**
     * @return ProductType|null
     */
    public function getType() : ?ProductType
    {
        return $this->type;
    }

    /**
     * @param ProductType|null $type
     */
    public function setType(?ProductType $type) : void
    {
        $this->type = $type;
    }

    /**
     * @return Collection|Documentation[]
     */
    public function getDocumentations(): Collection
    {
        return $this->documentations;
    }
}