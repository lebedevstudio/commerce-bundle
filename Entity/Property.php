<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;
use Doctrine\ORM\Mapping as ORM;
use lst\MediaBundle\Entity\File;

/**
 * Property Entity
 *
 * @ORM\Table(
 *     name="commerce_properties",
 *     indexes={
 *          @ORM\Index(name="IDX_COMMERCE_PROPERTIES_LOCALE_INDEX", columns={"locale"})
 *      }
 *  )
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\PropertyRepository")
 */
class Property extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 13;
    /** @var string */
    public const SINGLE_KEY = 'property';
    /** @var string */
    public const MULTIPLE_KEY = 'properties';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    private $title = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    private $measurement = '';

    /**
     * @Asserts\UniqueField(
     *     with="locale"
     * )
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic"})
     */
    private $path = '';

    /**
     * @Assert\Type(
     *     type="lst\MediaBundle\Entity\File"
     * )
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"image"})
     */
    private $image = null;

    /**
     * @Assert\Type(
     *     type="lst\CommerceBundle\Entity\PropertyGroup"
     * )
     * @ORM\ManyToOne(targetEntity="PropertyGroup", inversedBy="properties")
     * @Groups({"group"})
     */
    private $group = null;

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getMeasurement() : string
    {
        return $this->measurement;
    }

    /**
     * @param string $measurement
     */
    public function setMeasurement(string $measurement) : void
    {
        $this->measurement = $measurement;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return PropertyGroup|null
     */
    public function getGroup() : ?PropertyGroup
    {
        return $this->group;
    }

    /**
     * @param PropertyGroup|null $group
     */
    public function setGroup(?PropertyGroup $group): void
    {
        $this->group = $group;
    }
}
