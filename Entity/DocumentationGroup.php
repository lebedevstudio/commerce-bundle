<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DocumentationGroup Entity
 *
 * @ORM\Table(name="commerce_documentation_groups")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\DocumentationGroupRepository")
 */
class DocumentationGroup extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 24;
    /** @var string */
    public const SINGLE_KEY = 'documentationGroup';
    /** @var string */
    public const MULTIPLE_KEY = 'documentationGroups';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\Length(max=255)
     * @Assert\NotBlank()
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false
     * )
     *
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\OneToMany(
     *     targetEntity="lst\CommerceBundle\Entity\Documentation",
     *     mappedBy="group"
     *
     * )
     * @Groups({"documentation"})
     **/
    private $documentation;

    public function __construct()
    {
        $this->documentation = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Collection|Documentation[]
     */
    public function getDocumentation(): Collection
    {
        return $this->documentation;
    }
}