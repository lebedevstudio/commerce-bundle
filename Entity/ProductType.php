<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use DateTimeImmutable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product type Entity
 *
 * @ORM\Table(name="commerce_products_types")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\ProductTypeRepository")
 */
class ProductType extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 18;
    /** @var string */
    public const SINGLE_KEY = 'type';
    /** @var string */
    public const MULTIPLE_KEY = 'types';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    protected $title;

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }
}
