<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use DateTimeImmutable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\MediaBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product Vendor Entity
 *
 * @ORM\Table(name="commerce_products_vendors")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\ProductVendorRepository")
 */
class ProductVendor extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 17;
    /** @var string */
    public const SINGLE_KEY = 'vendor';
    /** @var string */
    public const MULTIPLE_KEY = 'vendors';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255)
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @Assert\Type(
     *     type="lst\MediaBundle\Entity\File"
     * )
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"image"})
     */
    private $image;

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image) : void
    {
        $this->image = $image;
    }
}