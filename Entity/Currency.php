<?php

declare(strict_types=1);

namespace lst\CommerceBundle\Entity;

use DateTimeImmutable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Currency Entity
 *
 * @ORM\Table(name="commerce_currencies")
 * @ORM\Entity(repositoryClass="lst\CommerceBundle\Repository\CurrencyRepository")
 * @UniqueEntity("iso")
 */
class Currency extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 5;
    /** @var string */
    public const SINGLE_KEY = 'currency';
    /** @var string */
    public const MULTIPLE_KEY = 'currencies';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=3
     * )
     * @ORM\Column(type="string", length=3, nullable=false, unique=true)
     * @Groups({"basic"})
     */
    protected $iso;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic"})
     */
    protected $title;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIso() : string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso) : void
    {
        $this->iso = $iso;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }
}
